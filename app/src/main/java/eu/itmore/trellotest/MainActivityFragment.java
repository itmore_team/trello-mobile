package eu.itmore.trellotest;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ExecutionException;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    ListView listViewToDo;
    ListView listViewDoing;
    ListView listViewDone;
    String boardID;
    SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_main, container, false);

        listViewToDo = (ListView)v.findViewById(R.id.listViewToDo);
        listViewDoing = (ListView)v.findViewById(R.id.listViewDoing);
        listViewDone = (ListView)v.findViewById(R.id.listViewDone);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boardID = sharedPreferences.getString("boardID", null);
//ysXTKrLZ
        if(boardID == null)
        {
            showDialog();
        }
        else
        {
            initBoard(boardID);
        }

        ((MainActivity)getActivity()).setOnMenuItemSelectedListener(new MainActivity.OnMenuItemSelectedListener() {
            @Override
            public void onMenuItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.setBoardIdItem:
                        showDialog();
                }
            }
        });

        return v;
    }

    private void initBoard(String id)
    {
        final TrelloClient client = new TrelloClient(getActivity());
        client.getBoard(id, new TrelloClient.OnRequestCompleteListener() {
            @Override
            public void onRequestComplete(String result) {

                TrelloBoard board = null;
                try
                {
                    board = new Gson().fromJson(result, TrelloBoard.class);
                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.invalid_board_id), Toast.LENGTH_SHORT).show();
                }

                if(board != null)
                {
                    getActivity().setTitle(board.getName());
                    ArrayList<TrelloBoard.TrelloCard> ToDoList = new ArrayList<TrelloBoard.TrelloCard>();
                    ArrayList<TrelloBoard.TrelloCard> DoingList = new ArrayList<TrelloBoard.TrelloCard>();
                    ArrayList<TrelloBoard.TrelloCard> DoneList = new ArrayList<TrelloBoard.TrelloCard>();

                    for (TrelloBoard.TrelloCard card : board.getCards()) {
                        if (card.getIdList().equals(board.getLists().get(0).getId())) //To do
                            ToDoList.add(card);
                        else if (card.getIdList().equals(board.getLists().get(1).getId())) //Doing
                            DoingList.add(card);
                        else if (card.getIdList().equals(board.getLists().get(2).getId())) //Done
                            DoneList.add(card);
                    }

                    listViewToDo.setAdapter(new TrelloListAdapter(getActivity(), board.getLists().get(0).getId(), ToDoList));
                    listViewDoing.setAdapter(new TrelloListAdapter(getActivity(), board.getLists().get(1).getId(), DoingList));
                    listViewDone.setAdapter(new TrelloListAdapter(getActivity(), board.getLists().get(2).getId(), DoneList));
                }
            }
        });
    }

    private void showDialog()
    {
        View input = View.inflate(getActivity(), R.layout.set_board_id_dialog, null);
        final EditText editText = (EditText)input.findViewById(R.id.editText);
        editText.setText(boardID == null ? "" : boardID);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.set_boardID);
        builder.setView(input);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                boardID = String.valueOf(editText.getText());
                sharedPreferences.edit().putString("boardID", boardID).apply();
                initBoard(boardID);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
}
