package eu.itmore.trellotest;

import java.util.ArrayList;

/**
 * Created by Krzysiek on 2015-05-27.
 */
public class TrelloBoard {

    private String id;
    private String name;
    private String desc;
    private ArrayList<TrelloList> lists;
    private ArrayList<TrelloCard> cards;
    private boolean closed;
    private String idOrganization;
    private String url;
    private TrelloPrefs prefs;

    public class TrelloPrefs {

        private String voting;
        private String permissionLevel;
        private String invitations;
        private String comments;

        public String getVoting()
        {
            return voting;
        }

        public String getPermissionLevel()
        {
            return permissionLevel;
        }

        public String getInvitations()
        {
            return invitations;
        }

        public String getComments()
        {
            return comments;
        }
    }

    public class TrelloList {

        private String id;
        private String name;

        public String getId()
        {
            return id;
        }

        public String getName()
        {
            return name;
        }
    }

    public class TrelloCard {

        private String id;
        private String idList;
        private String name;
        private int pos;

        public String getId()
        {
            return id;
        }

        public String getIdList()
        {
            return idList;
        }

        public String getName()
        {
            return name;
        }

        public int getPos()
        {
            return pos;
        }
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getDesc()
    {
        return desc;
    }

    public ArrayList<TrelloList> getLists()
    {
        return lists;
    }

    public ArrayList<TrelloCard> getCards()
    {
        return cards;
    }

    public boolean isClosed()
    {
        return closed;
    }

    public String getIdOrganization()
    {
        return idOrganization;
    }

    public String getUrl()
    {
        return url;
    }

    public TrelloPrefs getPrefs()
    {
        return prefs;
    }
}
