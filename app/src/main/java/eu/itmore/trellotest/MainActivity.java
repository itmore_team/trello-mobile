package eu.itmore.trellotest;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    private OnMenuItemSelectedListener listener = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(listener != null) {
            listener.onMenuItemSelected(item);
        }
        return true;
    }

    public void setOnMenuItemSelectedListener(OnMenuItemSelectedListener listener)
    {
        this.listener = listener;
    }

    public interface OnMenuItemSelectedListener
    {
        void onMenuItemSelected(MenuItem item);
    }
}
