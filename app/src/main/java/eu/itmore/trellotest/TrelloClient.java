package eu.itmore.trellotest;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krzysiek on 2015-05-27.
 */
public class TrelloClient {

    private Context context;

    public TrelloClient(Context context)
    {
        this.context = context;
    }

    public void getBoard(String id, OnRequestCompleteListener listener)
    {
        new ClientTaskGET().execute("https://api.trello.com/1/boards/" + id + "?lists=all&cards=all", listener);
    }

    public void getToken(OnRequestCompleteListener listener)
    {
        new ClientTaskGET().execute("https://trello.com/1/authorize?key=4fd38383f27ce6dc4318d12e8cf734a8&name=My_Board&expiration=never&response_type=token&scope=read,write", listener);
    }

    public void editCard(String id, String param, String value, OnRequestCompleteListener listener)
    {
        try {
            new ClientTaskPOST().execute("https://api.trello.com/1/cards/" + id + "/" + param + "?value=" + URLEncoder.encode(value, "utf-8"), null, listener);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void addCard(String idList, String name, String desc,OnRequestCompleteListener listener)
    {
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("idList", idList));
            nameValuePairs.add(new BasicNameValuePair("name", URLEncoder.encode(name, "utf-8")));
            nameValuePairs.add(new BasicNameValuePair("desc", URLEncoder.encode(desc, "utf-8")));
            nameValuePairs.add(new BasicNameValuePair("key", "4fd38383f27ce6dc4318d12e8cf734a8"));
            new ClientTaskPOST().execute("https://api.trello.com/1/cards", nameValuePairs, listener);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private class ClientTaskGET extends AsyncTask<Object, Void, String>
    {
        OnRequestCompleteListener listener = null;

        @Override
        protected String doInBackground(Object... objects) {

            String url = (String)objects[0];
            listener = (OnRequestCompleteListener)objects[1];
            String result = null;
            HttpClient client = new DefaultHttpClient();
            try {
                HttpResponse response = client.execute(new HttpGet(url));
                HttpEntity entity = response.getEntity();
                if(entity != null)
                {
                    InputStream is = entity.getContent();
                    result = streamToString(is);
                    is.close();
                }
            } catch (IOException e) {

            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(listener != null)
            {
                listener.onRequestComplete(result);
            }

            if(result != null) {
                Log.d("TrelloClient", result);
            }
            else {
                Log.e("TrelloClient", "Result is NULL");
            }
        }
    }

    private class ClientTaskPOST extends AsyncTask<Object, Void, String>
    {
        OnRequestCompleteListener listener = null;

        @Override
        protected String doInBackground(Object... objects) {

            String url = (String)objects[0];
            List<NameValuePair> nameValuePairs = (List<NameValuePair>)objects[1];
            listener = (OnRequestCompleteListener)objects[2];
            String result = null;
            HttpClient client = new DefaultHttpClient();
            try {
                HttpPost post = new HttpPost(url);

                if(nameValuePairs != null)
                    post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                if(entity != null)
                {
                    InputStream is = entity.getContent();
                    result = streamToString(is);
                    is.close();
                }
            } catch (IOException e) {

            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(listener != null)
            {
                listener.onRequestComplete(result);
            }

            if(result != null) {
                Log.d("TrelloClient", result);
            }
            else {
                Log.e("TrelloClient", "Result is NULL");
            }
        }
    }

    private String streamToString(InputStream is)
    {
        String result = "";
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
        String line = null;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                result += line + "\n";
            }
        }
        catch (Exception e) {

        }
        return result;
    }

    public interface OnRequestCompleteListener
    {
        void onRequestComplete(String result);
    }
}
