package eu.itmore.trellotest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Krzysiek on 2015-05-28.
 */
public class TrelloListAdapter extends ArrayAdapter {

    private Context context;
    private String idList;
    private ArrayList<TrelloBoard.TrelloCard> cards;

    public TrelloListAdapter(Context context, String idList, ArrayList<TrelloBoard.TrelloCard> cards) {
        super(context, android.R.layout.simple_list_item_1, cards);
        this.context = context;
        this.idList = idList;
        this.cards = cards;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if(v == null)
            v = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1, parent, false);

        TextView textView = (TextView)v.findViewById(android.R.id.text1);
        textView.setText(cards.get(position).getName());

        return v;
    }
}
