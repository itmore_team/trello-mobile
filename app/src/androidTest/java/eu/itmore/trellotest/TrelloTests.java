package eu.itmore.trellotest;

import android.app.Application;
import android.test.ApplicationTestCase;

import com.google.gson.Gson;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Krzysiek on 2015-05-29.
 */
public class TrelloTests extends ApplicationTestCase<Application> {

    public TrelloTests() {
        super(Application.class);
    }

    //Trello tests require Internet connection

    public void testGetBoardWithCorrectBoardIDReturnsTrelloBoard() throws InterruptedException {
        final CountDownLatch signal = new CountDownLatch(1);
        new TrelloClient(getContext()).getBoard("ysXTKrLZ", new TrelloClient.OnRequestCompleteListener() {

            @Override
            public void onRequestComplete(String result) {

                assertNotNull(result);
                try
                {
                    TrelloBoard board = new Gson().fromJson(result, TrelloBoard.class);
                    assertNotNull(board);
                }
                catch (Exception ex)
                {
                    assertEquals(true, false);
                }
                signal.countDown();
            }
        });
        signal.await();
    }

    public void testGetBoardWithIncorrectBoardIDReturnsInvalidIDMessage() throws InterruptedException {
        final CountDownLatch signal = new CountDownLatch(1);
        new TrelloClient(getContext()).getBoard("asdfghjkasdf", new TrelloClient.OnRequestCompleteListener() {

            @Override
            public void onRequestComplete(String result) {

                try
                {
                    TrelloBoard board = new Gson().fromJson(result, TrelloBoard.class);
                    assertEquals(true, false);
                }
                catch (Exception ex)
                {
                    assertEquals(true, true);
                }
                signal.countDown();
            }
        });
        signal.await();
    }
}
