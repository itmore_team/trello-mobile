#Trello Test

TrelloTest to aplikacja przeznaczona na platform� Android, s�u��ca do wy�wietlania tablic istniej�cych w systemie Trello.

#Instalacja

W celu instalacji aplikacji, nale�y wykona� nast�puj�ce kroki:
1. Pobra� plik TrelloTest.apk z repozytorium (plik znajduje si� w g��wnym folderze)
2. Skopiowa� plik apk do pami�ci telefonu
3. Zainstalowa� plik apk na telefonie

#Obs�uga

Przy pierwszym uruchomieniu aplikacji wy�wietli si� okienko, do kt�rego nale�y wpisa� 8-cyfrowe skr�cone ID tablicy Trello (https://trello.com/b/ **ysXTKrLZ** /my-board).
Je�li ID jest poprawne to ekranie wy�wietl� si� listy oraz zawarte w nich karty pobrane z systemu Trello. Aby zmodyfikowa� ID tablicy, nale�y wcisn�� przycisk menu i wybra� opcj� "Set Board ID", nast�pnie pojawi si� wy�ej opisane okienko.